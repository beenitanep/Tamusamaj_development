<?php 
/*
Template Name: Events
*/
get_header();?>
    <section class="content inner-content">
      <div class="container">
        <div class="row mb-20">
          <div class="col">
            <h2 class="innertitle">Events</h2>
          </div>
          <div class="col text-right">
            <a href="<?php bloginfo('url');?>" class="breadcrumb">Back to home</a>
          </div>
        </div>
        <div class="row">
	<?php
      $args = array(
      'posts_per_page' => -1,
      'post_type' => 'event'               
      );
    $counter = 0;
    query_posts($args);
    global $wp_query;
    $count_post = $wp_query->post_count;
    while (have_posts()) : the_post();
    $counter++;
    ?>
          <div class="col-lg-3 col-md-3">
            <article class="blogpost">
            <?php 
          if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'blog');?>
              <div class="blog-img">
                <img src="<?php echo $image[0];?>" />
              </div>
              <?php }?>
              <div class="blogpost-body">
                <div class="blog-title">
                  <h2 class="title">
                    <a href="<?php the_permalink();?>"><?php the_title();?></a>
                  </h2>
                  <div class="post-info">
                    <span><?php echo get_the_date('Y-m-d'); ?> <?php the_time( 'H:i:s' ); ?></span>
                  </div>
                 <!--  <div class="submitted">by 
                  <a href="#">India</a></div> -->
                </div>
                <div class="blogpost-content">
                  <div class="Description"><?php the_content();?></div>
                </div>
                <div class="blog-footer clearfix">
                  <!-- <div class="pull-left">
                    <a href="#"><?=$Count?> comments</a>
                  </div> -->
                  <div class="blog-btn pull-right">
                    <a href="<?php the_permalink();?>" class="links">Read More</a>
                  </div>
                </div>
              </div>
            </article>
          </div>
          
	<?php  
            endwhile;  
 wp_reset_query();
     ?> 
        </div>
      </div>
    </section>
    <?php get_footer();?>
