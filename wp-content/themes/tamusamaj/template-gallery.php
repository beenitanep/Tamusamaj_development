<?php 
/*
Template Name: Gallery
*/
get_header();?>
<section class="content inner-content">
	<div class="container">
		<div class="row mb-20">
			<div class="col">
				<h2 class="innertitle">Gallery</h2>
			</div>
			<div class="col text-right">
				<a href="<?php bloginfo('url');?>" class="breadcrumb">
					<i class="fa fa-home"></i> Back to home
				</a>
				
			</div>
		</div>
		<div id="gallery" style="display:none;">
		 <?php
      $args = array(
      'posts_per_page' => -1,
      'post_type' => 'gallery'               
      );
    $counter = 0;
    query_posts($args);
    global $wp_query;
    $count_post = $wp_query->post_count;
    while (have_posts()) : the_post();
    $counter++;
    ?>
    <?php 
					if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full-size');?>
			<a href="#">
				<img alt="<?php the_title();?>"
				src="<?php echo $image[0];?>" 
				data-image="<?php echo $image[0];?>" 
				data-image-mobile="<?php echo $image[0];?>" 
				data-thumb-mobile="<?php echo $image[0];?>" 
				data-description=""
				style="display:none"> 
			</a>
		<?php  
		 }
            endwhile;  
 wp_reset_query();
     ?> 
		</div>
	</div>
</section>

 <?php get_footer();?>