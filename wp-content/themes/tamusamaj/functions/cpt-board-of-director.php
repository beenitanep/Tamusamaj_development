<?php 
/*=======================================================================*/
// Event  Post Type
/*=======================================================================*/
add_action('init', 'register_board_of_director');
function register_board_of_director(){
	$labels = array(
		'name' => _x('Board Of Director', 'post type general name'),
		'singular_name' => _x('Board Of Director', 'post type singular name'),
		'add_new' => _x('Add New', 'Board Of Director'),
		'add_new_item' => __('Board Of Director'),
		'edit_item' => __('Edit Board Of Director'),
		'new_item' => __('New Board Of Director'),
		'view_item' => __('View Board Of Director'),
		'search_items' => __('Search Board Of Director'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''

					);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		//'menu_icon' => get_stylesheet_directory_uri() . '/images/slider-icon.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		//'menu_position' => '',
		'supports' => array('title','thumbnail')
				);
	register_post_type('board_of_director' , $args);

}



?>