<?php 
/*=======================================================================*/
// Slider  Post Type
/*=======================================================================*/
add_action('init', 'register_advertise');
function register_advertise(){
	$labels = array(
		'name' => _x('Advertise', 'post type general name'),
		'singular_name' => _x('Advertise', 'post type singular name'),
		'add_new' => _x('Add New', 'Advertise'),
		'add_new_item' => __('Advertise'),
		'edit_item' => __('Edit Advertise'),
		'new_item' => __('New Advertise'),
		'view_item' => __('View Advertise'),
		'search_items' => __('Search Advertise'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''

					);
	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		//'menu_icon' => get_stylesheet_directory_uri() . '/images/slider-icon.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => false,
		//'menu_position' => '',
		'supports' => array('title','thumbnail')
				);
	register_post_type('advertise' , $args);

	$object_type=array("advertise");
	$labels = array(
		'name' => _x( 'Category', 'taxonomy general name' ),
		'singular_name' => _x( 'Category', 'taxonomy singular name' ),
		'search_items' =>  __( 'Search Category' ),
		'all_items' => __( 'All Category' ),
		'parent_item' => __( 'Parent Category' ),
		'parent_item_colon' => __( 'Parent Category:' ),
		'edit_item' => __( 'Edit Category' ), 
		'update_item' => __( 'Update Category' ),
		'add_new_item' => __( 'Add New Category' ),
		'new_item_name' => __( 'New Category Name' ),
		'menu_name' => __( 'Category' ),
	);
	$args=array(
		"hierarchical" => true,
		"labels" => $labels,
		"show_ui" => true,
		"query_var" => true
		
	);
	register_taxonomy('advertisecat', $object_type, $args);

}



?>