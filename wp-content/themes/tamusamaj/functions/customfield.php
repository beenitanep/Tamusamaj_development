<?php 
add_action('add_meta_boxes', 'register_meta_box');
add_action('save_post', 'save_meta_box');

function register_meta_box(){
    //adding url customfield for slider custompost type
     add_meta_box( 'url', __( 'Url', 'Url' ), 'url_display_callback', 'slider' );
     add_meta_box( 'designation', __( 'Designation', 'Designation' ), 'designation_display_callback', 'board_of_director' );
     add_meta_box( 'link', __( 'Link', 'Link' ), 'link_display_callback', 'video' );
     $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;

    }

    function url_display_callback( $post ) 
    {

        $url = get_post_meta( $post->ID, 'url', true );

        $outline = '';

        $outline .= '<label for="url">'. __('Url', 'wp') .'</label>';
        $outline .= '<input type="text" name="url" id="url" value="'. esc_attr($url) .'" />';
     
        echo $outline;

    }

    function designation_display_callback( $post ) 
    {

        $designation = get_post_meta( $post->ID, 'designation', true );

        $outline = '';

        $outline .= '<label for="designation">'. __('Designation', 'wp') .'</label>';
        $outline .= '<input type="text" name="designation" id="designation" value="'. esc_attr($designation) .'" />';
     
        echo $outline;

    }

    function link_display_callback( $post ) 
    {

        $link = get_post_meta( $post->ID, 'link', true );

        $outline = '';

        $outline .= '<label for="link">'. __('Link', 'wp') .'</label>';
        $outline .= '<input type="text" name="link" id="link" value="'. esc_attr($link) .'" />';
     
        echo $outline;

    }


    function save_meta_box( $post_id)
    {
        $url   = isset( $_POST['url'] ) ? $_POST['url'] : '';
        update_post_meta($post_id,'url',$url);

        $designation = isset( $_POST['designation'] ) ? $_POST['designation'] : '';
        update_post_meta($post_id,'designation',$designation);

        $link = isset( $_POST['link'] ) ? $_POST['link'] : '';
        update_post_meta($post_id,'link',$link);
    }