<?php 
/*
Template Name: Home
*/
get_header();?> 
  <!--Slider Start-->
  <div class="slider-wrapper">
   <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
     <div class="carousel-inner">
     <?php
     $args = array(
     'posts_per_page' => 5,
     'post_type' => 'slider'              
     );
   $counter = 0;
   query_posts($args);
   global $wp_query;
   $count_post = $wp_query->post_count;
   while (have_posts()) : the_post();
   $counter++;
   ?>
       <div class="carousel-item <?php echo ($counter==1?'active':'');?>">
       <?php 
       if(has_post_thumbnail()) {      
         $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'slider');
         ?>
        <img class="d-block w-100" src="<?php echo $image[0];?>" alt="<?php the_title();?>">
         <?php }?>
         <div class="carousel-caption d-none d-md-block">
           <h3><?php the_title();?></h3>
           <?php the_content();
             $urlval = get_post_meta( $post->ID, 'url', true );
             //if( $urlval != ''){
           ?>
           <a href="<?php echo $urlval; ?>" class="btn btn-outline">Start Exploring</a> </div>
           <?php //} ?>
         </div>
    <?php   endwhile;  
 wp_reset_query();
     ?> 
           </div>
           <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
         </div>


          <!--Slider End--> 
          <!-- Activities Start -->
          <section class="activities">
            <div class="container">
              <div class="row">
                <div class="col-lg-6 col-md-6">
                  <div class="act_item"> <i class="act_icon icon icon-Users"></i>
                  <?php
        $args = array(
          'post_type' => 'page',
          'p'=>47
          );
        $counter = 0; 
        query_posts($args);
        while (have_posts()) : the_post();
        ?>
                        <div class="act_content">
                          <h3><a href="<?php the_permalink();?>"><?php the_title();?></a></h3>
                         <?php the_content();?>
                          <a href="<?php the_permalink();?>" class="links">Join Now <i class="fa fa-long-arrow-right"></i></a></div>
         <?php   endwhile; 
        wp_reset_query(); 
        ?> 
                        </div>
                      </div>
                      <div class="col-lg-6 col-md-6">
                        <div class="act_item donate-wrapper"> <i class="act_icon icon icon-Dollars"></i>
                        <?php
        $args = array(
          'post_type' => 'page',
          'p'=>8
          );
        $counter = 0; 
        query_posts($args);
        while (have_posts()) : the_post();
        ?>
                        <div class="act_content">
                          <h3><a href="<?php the_permalink();?>"><?php the_excerpt();?></a></h3>
                          <?php the_content();?>
                          <a href="<?php the_permalink();?>" class="links">Donate Here<i class="fa fa-long-arrow-right"></i></a></div>
         <?php   endwhile; 
        wp_reset_query(); 
        ?> 
                        </div>
                        </div>
                      </div>
                    </div>
                  </section>
                  <!-- Activities End --> 
                  <!-- Content Start -->
                  <section class="content">
                    <div class="container">
                      <div class="row v-align-children">
                      <?php
        $args = array(
          'post_type' => 'page',
          'p'=>26
          );
        $counter = 0; 
        query_posts($args);
        while (have_posts()) : the_post();
        ?>
                        <div class="col-lg-5 col-md-6">
                          <div class="welcome_txt">
                            <h2><?php echo get_the_excerpt(26);?></h2>
                            <?php the_content();?>
                            <a href="<?php the_permalink();?>" class="btn">Read More</a> </div>
                          </div>
                          <div class="col-lg-7 col-md-6">
                            <div class="primary-box">
                            <?php 
        if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'home-about');
          ?>
                              <img class="primary-img" src="<?php echo $image[0];?>" alt="<?php the_title();?>">
          <?php }?>
                            </div>    
                         </div>
         <?php   endwhile; 
        wp_reset_query(); 
        ?> 
                            </div>
                            <?php
                      $args = array(
                  'post_type' => 'advertise',
                  'posts_per_page'=>1,
                  'tax_query' => array(
                    array(
                      'taxonomy' => 'advertisecat',
                      'field' => 'slug',
                      'terms' => 'body-advertise'
                  ))
                   );
        $counter = 0;
        query_posts($args);
        while (have_posts()) : the_post();
          $counter++;
                ?>  
                            <div class="advertise">
                            <?php 
        if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'advertise-body');
          ?>
                            <img src="<?php echo $image[0];?>" alt="<?php the_title();?>">
          <?php }
            endwhile; 
        wp_reset_query(); 
          ?>
                            </div>                            
                          </div>
                        </section>
                        <!-- Content End --> 
                        <!-- Project Start -->
                        <section class="content pt-0">
                          <div class="container">
                          <div class="row">
                            <div class="col-lg-4 col-md-4">
                            <?php
        $args = array(
          'post_type' => 'page',
          'p'=>20
          );
        $counter = 0; 
        query_posts($args);
        while (have_posts()) : the_post();
        ?>
                            <div class="title">
                              <h3><?php the_excerpt();?></h3>
                             </div>
                                <?php the_content();?>
                                <a href="<?php the_permalink();?>" class="btn">Read More</a>
        <?php 
            endwhile; 
        wp_reset_query(); 
          ?>
                             </div>
                             <div class="col-lg-4 col-md-4">
                              <?php
                      $args = array(
                  'post_type' => 'advertise',
                  'posts_per_page'=>1,
                  'tax_query' => array(
                    array(
                      'taxonomy' => 'advertisecat',
                      'field' => 'slug',
                      'terms' => 'event-left-advertise'
                  ))
                   );
        $counter = 0;
        query_posts($args);
        while (have_posts()) : the_post();
          $counter++;
          if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'small-advertise');
                ?>  
                            <div class="advertise">
                              <img src="<?php echo $image[0];?>" alt="<?php the_title();?>">
                            </div>
          <?php 
        }
            endwhile; 
        wp_reset_query(); 
          ?>           
                               
                             </div>
                             <div class="col-lg-4 col-md-4">
                                <div class="right-sidebar">
                                  <div class="side_block">
                                    <h4><i class="fa fa-calendar"></i>Latest Events</h4>
                                    <?php
      $args = array(
      'posts_per_page' => 3,
      'post_type' => 'event'               
      );
    $counter = 0;
    query_posts($args);
    global $wp_query;
    $count_post = $wp_query->post_count;
    while (have_posts()) : the_post();
    $counter++;
    ?>
                                    <div class="event_item">
                                        <span class="event_img"><img data-src="holder.js/50x50?bg=000000" class="rounded-circle"></span>
                                        <div class="event_detail">
                                        <h5><a href="<?php the_permalink();?>"><?php the_title();?></a></h5>
                                        <span class="date"><?php echo get_the_date('m d,Y'); ?></span>
                                        <p><?php echo substr(strip_tags(get_the_excerpt()),0,75);?>...</p>
                                        </div>
                                    </div>
                                     <?php   endwhile;  
wp_reset_query();
      ?> 
                                  </div>
                                </div>
                             </div>
                             </div>
                             <?php
                      $args = array(
                  'post_type' => 'advertise',
                  'posts_per_page'=>1,
                  'tax_query' => array(
                    array(
                      'taxonomy' => 'advertisecat',
                      'field' => 'slug',
                      'terms' => 'footer-advertise'
                  ))
                   );
        $counter = 0;
        query_posts($args);
        while (have_posts()) : the_post();
          $counter++;
          if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'advertise-body');
                ?> 
                               <div class="advertise">
                            <img src="<?php echo $image[0];?>" alt="<?php the_title();?>">
                            </div>  
                             <?php 
        }
            endwhile; 
        wp_reset_query(); 
          ?> 
                                  </div>
                                </section>
<?php get_footer();?>