<?php 
/*=======================================================================*/
// Wordpress Custom Functions
/*=======================================================================*/

/*=======================================================================*/
// Enqueue Scripts
/*=======================================================================*/


function wpdocs_theme_name_scripts() {
	wp_enqueue_style( 'bootstrap.min', get_template_directory_uri() . '/css/bootstrap.min.css',false,'1.1','all');
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/fonts/font-awesome/css/font-awesome.min.css',false,'1.1','all');
	wp_enqueue_style( 'animate', get_template_directory_uri() . '/css/animate.css',false,'1.1','all');
	wp_enqueue_style( 'bootstrap-dropdownhover', get_template_directory_uri() . '/css/bootstrap-dropdownhover.min.css',false,'1.1','all');
	wp_enqueue_style( 'stroke-gap', get_template_directory_uri() . '/css/stroke-gap-icon.css',false,'1.1','all');
	wp_enqueue_style( 'unite-gallery', get_template_directory_uri() . '/css/unite-gallery.css',false,'1.1','all');
	wp_enqueue_style( 'magnific-popup', get_template_directory_uri() . '/css/magnific-popup.css',false,'1.1','all');
	wp_enqueue_style( 'style', get_template_directory_uri() . '/css/style.css',false,'1.1','all');
	wp_enqueue_style( 'responsive', get_template_directory_uri() . '/css/responsive.css',false,'1.1','all');
    wp_enqueue_script( 'jquery-min', get_template_directory_uri() . '/js/jquery.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'popper', get_template_directory_uri() . '/js/popper.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'bootstrap-min-js', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'holder', get_template_directory_uri() . '/js/holder.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'unitegallery', get_template_directory_uri() . '/js/unitegallery.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'ug-theme', get_template_directory_uri() . '/js/ug-theme-tiles.js', array(), '1.0.0', true );
     wp_enqueue_script( 'magnific', get_template_directory_uri() . '/js/jquery.magnific-popup.min.js', array(), '1.0.0', true );
      wp_enqueue_script( 'script', get_template_directory_uri() . '/js/script.js', array(), '1.0.0', true );
	}
	add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );

/*
	=====================================================
	Custom Menus
	=====================================================
*/
add_action( 'init', 'register_my_menus' );
function register_my_menus() {
	register_nav_menus(
		array(
			'primary'   => __('Primary Navigation'),
			'secondary' => __('Secondary Navigation')
		)
	);
} 

//Include Nav Walker class_alias to support bootsrap 4
    require( get_template_directory().'/bs4navwalker.php');


/*=======================================================================*/
// Including Functions Files
/*=======================================================================*/
require_once(TEMPLATEPATH . '/functions/cpt-slider.php');
require_once(TEMPLATEPATH . '/functions/cpt-advertise.php');
require_once(TEMPLATEPATH . '/functions/cpt-event.php');
require_once(TEMPLATEPATH . '/functions/cpt-board-of-director.php');
require_once(TEMPLATEPATH . '/functions/cpt-gallery.php');
require_once(TEMPLATEPATH . '/functions/cpt-video.php');
require_once(TEMPLATEPATH . '/functions/customfield.php');



/*=======================================================================*/
// Image Thumbnails
/*=======================================================================*/
if ( function_exists( 'add_image_size' ) ) { 
	add_image_size( 'slider', 1366, 604,true );
	add_image_size( 'home-about', 635, 476,true );
	add_image_size( 'advertise-body', 1110, 208,true );
	add_image_size( 'small-advertise', 350, 292,true );
	add_image_size( 'home-events', 50, 50,true );
	add_image_size( 'page-image', 250, 167,true );
	//add_image_size( 'board-member', 350, 197,true );
	add_image_size( 'blog', 255, 191,true );
	add_image_size( 'home-gallery', 273, 182,true );
  }

 /*=======================================================================*/
// Image Thumbnails (add feature image) 
//post thumbnails enabled
/*=======================================================================*/
add_theme_support( 'post-thumbnails' );

/*=======================================================================*/
// Page excerpt support
/*=======================================================================*/

add_post_type_support('page', 'excerpt');
		?>