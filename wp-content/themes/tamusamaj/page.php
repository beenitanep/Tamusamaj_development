<?php get_header();?> 
 <section class="content inner-content">
		<div class="container clearfix">
						<div class="row mb-20">
						<div class="col">
								<h2 class="innertitle"><?php the_title();?> </h2>
						</div>
							<div class="col text-right">
								<a href="<?php bloginfo('url');?>" class="breadcrumb">
										<i class="fa fa-home"></i> Back to home
								</a>
							</div>
						</div>
						 <?php
		if (have_posts()) : while (have_posts()) : the_post();
		?> 
						
						 <?php 
        if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'page-image');
          ?>
          				<div class="square-img">
						<img src="<?php echo $image[0];?>" alt=""></div>
		<?php }?>
						<?php the_content();?>
</div>
<?php endwhile; 
endif; ?> 
</section>
<?php get_footer();?>