<?php 
/*
Template Name: Contact Us
*/
get_header();?>
<section class="content inner-content">
  <div class="container">
    <div class="row mb-20">
      <div class="col">
        <h2 class="innertitle">Contact Us</h2>
      </div>
      <div class="col text-right"> <a href="<?php bloginfo('url');?>" class="breadcrumb"> <i class="fa fa-home"></i> Back to home </a> </div>
    </div>
    <div id="map">
      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3515.6815182055498!2d83.9840814144563!3d28.216987209518372!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3995944d76693d11%3A0xf1f006d89be13640!2sNew+Rd%2C+Pokhara+33700!5e0!3m2!1sen!2snp!4v1507273557669" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
    <div class="row">
      <div class="col-lg-6 col-md-6 mt-20">
        <div class="contact_address">
          <h2 class="innertitle">Pal Ewam Namgyal Monastic School</h2>
          <ul>
            <li><i class="fa fa-map-marker"></i><b>Address : </b> Pokhara Lekhnath 18, Bhakunde, Kaski, Nepal </li>
            <li><i class="fa fa-phone"></i><b>Phone : </b> +977-061-622629 </li>
            <li><i class="fa fa-envelope"></i><b>Email : </b> <a href="mailto:info@penms.edu.np">info@penms.edu.np</a> </li>
            <li><i class="fa fa-globe"></i><b>Website : </b> <a href="http://penms.edu.np/" target="_blank">www.penms.edu.np</a>
</li>
            
          </ul>
          <hr>
          <h2 class="innertitle">Address of Mustang</h2>
          <ul>
            <li><i class="fa fa-map-marker"></i><b>Address : </b> Chhonup 1, Namgyal, Mustang, Nepal
</li>
            <li><i class="fa fa-phone"></i><b>Phone : </b> +977-061-622629 </li>
            <li><i class="fa fa-envelope"></i><b>Email : </b> <a href="mailto:info@penms.edu.np">info@penms.edu.np</a> </li>
            <li><i class="fa fa-globe"></i><b>Website : </b> <a href="http://penms.edu.np/" target="_blank">www.penms.edu.np</a>
</li>
            
          </ul>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 mt-20">
      	 <h2 class="innertitle">Get In Touch</h2>
         <p class="lead">Please fill up the required(<span class="red">*</span>) field.</p>
         <div class="contact-form">
			
			<?php echo do_shortcode( '[contact-form-7 id="98" title="Contact Form" html_class="row"]' ); ?>
		
		</div>
      </div>
    </div>
  </div>
</section>
<?php get_footer();?>