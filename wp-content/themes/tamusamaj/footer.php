 <footer class="footer">
                                      <div class="container">
                                        <div class="row">
                                          <div class="col-md-3 col-sm-3">
                                            <div class="footer-content">
                                              <div class="footer-logo">
                                              <?php
        $args = array(
          'post_type' => 'page',
          'p'=>26
          );
        $counter = 0; 
        query_posts($args);
        while (have_posts()) : the_post();
        ?>
                                                <h4><?php the_title();?></h4>
                                                <p><?php echo substr(strip_tags(get_the_content()),0,107);?></p>
        <?php   endwhile; 
        wp_reset_query(); 
        ?> 
                                                <a href="<?php echo get_the_permalink('26');?>" class="links">Read More</a> </div>
                                              </div>
                                            </div>
                                            <div class="col-md-9 col-sm-9">
                                              <div class="row">
                                                <div class="col-md-4 col-sm-4">
                                                  <h4>Connect</h4>
                                                  <ul>
                                                    <li><a href="#">+977-532305</a></li>
                                                    <li><a href="#">info@kolmabahakotpokhara.org</a></li>
                                                    <li>Pokhara Lekhnath -10, Indra Marga, Kaski, Nepal</li>
                                                  </ul>
                                                </div>
                                                <div class="col-md-3 col-sm-3">
                                                  <h4>Social Media</h4>
                                                  <ul class="social-links">
                                                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                  </ul>
                                                </div>
                                                <div class="col-md-5 col-sm-5">
                                                  <h4>Subscribe to our newsletter</h4>
                                                  <small>We will send you updates on how our team working on new projects and general news.</small>
                                                  <div class="newsletter">
                                                    <!-- <form>
                                                      <div class="form-group">
                                                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                                                      </div>
                                                      <button type="submit" class="btn btn-primary">Subscribe</button>
                                                    </form> -->
                                                    
                                                    <!-- Begin MailChimp Signup Form -->
<link href="//cdn-images.mailchimp.com/embedcode/slim-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">
  #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
  /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
     We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
</style>
<div id="mc_embed_signup">
<form action="//slr.us14.list-manage.com/subscribe/post?u=f4e5751231b2ec7529c0d4649&amp;id=d36babcd4b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
  
  <input type="email" value="" name="EMAIL" class="email form-control" id="mce-EMAIL" placeholder="Enter email" required>
    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_f4e5751231b2ec7529c0d4649_d36babcd4b" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button btn btn-primary"></div>
    </div>
</form>
</div>

<!--End mc_embed_signup-->
                                                  </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div class="bottom-footer text-center ">
                                          <div class="container clearfix">
                                            <p class="copyright float-left">© 2017 Kolma Bahakot Tamu Samaj. All rights reserved</p>
                                            <p class="float-right">Design by <a href="http://webpagenepal.com/" target="_blank">Webpage Nepal</a></p>
                                          </div>
                                        </div>
                                      </footer>
                                      <?php wp_footer(); ?>
                                    </body>
                                    </html>