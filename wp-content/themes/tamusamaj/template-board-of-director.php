<?php 
/*
Template Name: Board Of Director
*/
get_header();?>
<section class="content inner-content">
  <div class="container">
    <div class="row mb-20">
      <div class="col">
 <h2 class="innertitle">Board of Directors </h2>
      </div>
      <div class="col text-right"> <a href="<?php bloginfo('url');?>" class="breadcrumb"> 
      <i class="fa fa-home"></i> Back to home </a> </div>
    </div>
    <div class="teamList">
    	<div class="row">
        <?php
      $args = array(
      'posts_per_page' => -1,
      'post_type' => 'board_of_director'               
      );
    $counter = 0;
    query_posts($args);
    global $wp_query;
    $count_post = $wp_query->post_count;
    while (have_posts()) : the_post();
    $counter++;
    ?>
        	<div class="col-lg-3 col-md-3">
            	<div class="team">
            	<?php 
					if(has_post_thumbnail()) {      
          $image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'blog');?>
                	<div class="teamImg"> 
                	<img src="<?php echo $image[0];?>">
                	</div>
                	<?php }?>
                    <div class="teamInfo"><h3><?php the_title();?></h3>
                    <?php 
                         $designation = get_post_meta( $post->ID, 'designation', true );
            if( $designation != ''){
                         ?>
                    	<h4><?php echo $designation;?></h4>    
                    	<?php } ?>                    
                    </div>                    
                </div>
            </div>
           <?php  
            endwhile;  
 wp_reset_query();
     ?> 
           
        </div>
    </div>
  </div>
</section>
<?php get_footer();?>