<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'tamusamaj_new');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'h<jsZ$n5,d~A/.`ouk.9>A<B`GhW>)Kb!%>$mK>vX%c*-57(YO%wFuen@q3{WHj`');
define('SECURE_AUTH_KEY',  'z0MC>4M%TMSwJ~HO}@rV*lJP[D#s,WTi3rS,@s QFc6l<M:)dx,/XR,$4(VQfyft');
define('LOGGED_IN_KEY',    'C_mJ%.g>0_7E0K5xs/,04v.M{24F&`0J2Tkub.G1~#BL@5MA5i+cY+=Zt7]_g6]v');
define('NONCE_KEY',        's+zd{A.|&rI~M|<uU}OV4{SG`yYqU<<V))bp^<DdddK%9X9|d#9:uwLg)nM3Mmwc');
define('AUTH_SALT',        ',5(_!uK[v|ROyF kmhDlR2`g00#p_ T/V?X&*J~fZ}.C8GlVHC[|~CgGG{`}D{]`');
define('SECURE_AUTH_SALT', 'aYmBfNm.~QoLhIWZ^y|Vo&9Cz*X%(g~qhvUN*UN5]:m#ZZn|_>ce`Pf|p?$crJgj');
define('LOGGED_IN_SALT',   'NsYjz=1Rtn(~kCK<hZzj1!|V;M8oVDp]Rq2n~!LOE2NYRJh3PrfY$ris,dG9gf{~');
define('NONCE_SALT',       'S6p(R1~8tKt`WEiWcqWJ&%euG]9fjA+KD4q~<DNZjdAs)vw[Lj5 f/c7K5xZF6@k');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'ts_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
